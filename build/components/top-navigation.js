import { html, PolymerElement } from "../node_modules/@polymer/polymer/polymer-element.js";

class TopNavigation extends PolymerElement {
  static get template() {
    return html`
		<style>
			.inline-list {
			overflow: hidden;
		}
		.inline-list li {
			display: block;
			float: left;
			margin-right: 15px;
			margin-bottom: 5px;		
		}

		@media only screen and (max-width: 768px) {
			.inline-list > li:first-child {
				margin-left: 0;
			}
		}
		@media only screen and (min-width: 768px) {
			.inline-list > li {
					display: block;
					float: left;
					margin-left: 15px;
			}
			.inline-list > li:first-child {
				margin-left: 0;
			}
		}
		
		.page-section {
			padding: 10px;
			margin: 0 auto;
			background: #e3e3e3;
		}
		</style>
		
		<ul class="inline-list">
			<li><a href="#home">Home</a></li>
			<li><a href="#portfolio">Portfolio</a></li>
			<li><a href="#contact">Contact</a></li>
			<li><a href="#about">About</a></li>
		</ul>
		
		<div id="home" class="page-section">
			<h1>Home</h1>
		</div>		
	  
    `;
  }

}

window.customElements.define('top-navigation', TopNavigation);
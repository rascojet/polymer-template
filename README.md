# Overview
This is a basic template to create web apps with Polymer.js 3.0. 

# Quick Overview
- Install Node.js and npm [https://nodejs.org/en/download]
- Download or clone this repository to your development environment [https://github.com/rascojet/polymer-template.git]
- Run npm install to install the project dependencies [npm install].
- Open command line and change directory to the app folder [cd polymer-template].
- Run the app [npm run serve]
- Build the app [npm run build]

# Demo
http://www.rascojet.com/github/polymer-template


## License
[MIT](http://opensource.org/licenses/MIT)